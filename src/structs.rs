use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug)]
pub struct Message {
    from: String,
    message: String,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Inbox {
    messages: Vec<Message>,
}
