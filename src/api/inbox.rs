use mongodb::{
    options::{ClientOptions, FindOptions},
    sync::Client,
};

use crate::structs::Message;

pub fn inbox() -> warp::reply::Json {
    let client_options = ClientOptions::parse("mongodb://localhost:27017").unwrap();
    let client = Client::with_options(client_options).unwrap();

    let database = client.database("dribo");
    let collection = database.collection("messages");

    let inbox = collection.find(None, FindOptions::default()).unwrap();
    let inbox: Vec<Message> = inbox.map(|v| v.unwrap()).collect();

    warp::reply::json(&inbox)
}
