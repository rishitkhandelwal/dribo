use crate::structs::Message;
use mongodb::{
    bson::to_document,
    options::{ClientOptions, InsertOneOptions},
    sync::Client,
};

pub fn message(body: Message) -> String {
    let client_options = ClientOptions::parse("mongodb://localhost:27017").unwrap();
    let client = Client::with_options(client_options).unwrap();

    println!("{:?}", body);
    let database = client.database("dribo");
    let collection = database.collection("messages");
    collection
        .insert_one(to_document(&body).unwrap(), InsertOneOptions::default())
        .unwrap();

    "sent message".to_string()
}
