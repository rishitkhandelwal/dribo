use dribo::api;
use dribo::structs::*;
use warp::Filter;

#[tokio::main]
async fn main() {
    let ping = warp::path!("ping").map(|| "pong");

    let inbox = warp::path!("api" / "inbox").map(api::inbox);
    let message = warp::path!("api" / "message")
        .and(warp::body::json::<Message>())
        .map(api::message);

    let get_routes = warp::get().and(ping.or(inbox));
    let post_routes = warp::post().and(message);

    let routes = warp::any().and(get_routes.or(post_routes));

    warp::serve(routes).run(([127, 0, 0, 1], 8000)).await;
}
